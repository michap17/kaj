# Semestrální práce z předmětu Klientské aplikace v Javascriptu

Tématem této semestrální práce je single-page aplikace neexistujícího kina, která umožňuje prohlížení nadcházejících promítání filmů, rezervaci lístku na konkrétní promítání včetně výběru konkrétního sedadla, filtrování a vyhledávání v tabulce filmů podle data nebo názvu filmu.
Veškeré rezervace jsou uloženy pouze v localStorage prohlížeče.

### Funkce

- Prohlížení nadcházejících promítání až měsíc dopředu. Jednotlivá promítání se načítají podle potřeby (lazy loading).
- Filtrování promítání podle data, názvu promítání a roku natočení filmu.
- Rezervace lístku na promítání, výběr libovolného počtu sedadel v grafickém plánu promítacího sálu a následné uložení rezervace do localStorage prohlížeče. 
- Po výběru sedadla již nelze dané sedadlo znovu vybrat, dokud nedojde ke zrušení rezervace. 
- Při rezervaci jsou náhodně vygenerována zabraná sedadla pro libovolné promítání, ty jsou poté uloženy v localStorage.

### Použité technologie
- Moment.js (https://momentjs.com/)
    - Knihovna pro formátování a práci s daty a časem
- Less (http://lesscss.org/)
    - CSS preprocesor
- Autoprefixer (https://autoprefixer.github.io/)
    - CSS vendor prefixer 
- SVG obrázky z https://gallery.manypixels.co/
- Ikony z https://material.io/tools/icons/

### Dokumentace Javascriptu
| Třída / funkce | Popis
| --- | --- |
| Screenings | Třída pro vytvoření rozvrhu promítání, filtrování a následného zobrazení
| Dialog | Třída pro zobrazení jednoduchého dialogu, který obsahuje hlavičku a obsah
| MultiStepDialog | Třída pro zobrazení dialogu s více kroky. Využito pro rezervaci promítání.
| Order | Třída pro zapamatování si všech zarezerovovaných sedadel v rezervaci.
| Seating | Třída pro správu sedadel pro nějaké datum
| screeningExpired | Vrátí, zda již promítání proběhlo
| generateScreeningPlan | Vytvoří z rozvrhu promítání HTML tabulku
| generateSeatingPlan | Vytvoří plán sedadel promítacího sálu včetně náhodného zabrání sedadel
| buySeat | Funkce, která se zavolá po kliknutí na konkrétní sedadlo v plánu a která aktualizuje rezervaci.
| buyingSteps | Funkce, která pro index kroku vrátí obsah dialogu v daném kroku. 
| buyTicket | Funkce, která se zavolá, když si uživatel chce zarezerovovat nějaký film.
| updateReservationTable | Aktualizuje tabulku rezervací ve stránce.
| deleteReservation | Odstraní rezervaci ze stránky i z localStorage