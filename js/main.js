// JS plugin for date formatting
moment.locale("cs");

// Movie database
const movies = [{
        movie: "Shazam!",
        year: "2019",
        duration: moment.duration(132, "m")
    },
    {
        movie: "Aquaman",
        year: "2018",
        duration: moment.duration(143, "m")
    },
    {
        movie: "Wonder Woman",
        year: "2017",
        duration: moment.duration(141, "m")
    },
    {
        movie: "Captain Marvel",
        year: "2019",
        duration: moment.duration(124, "m")
    },
    {
        movie: "Kouzelný park",
        year: "2019",
        duration: moment.duration(85, "m")
    },
    {
        movie: "Hellboy",
        year: "2019",
        duration: moment.duration(121, "m")
    },
    {
        movie: "Penguins",
        year: "2019",
        duration: moment.duration(76, "m")
    },
    {
        movie: "Avengers: Endgame",
        year: "2019",
        duration: moment.duration(182, "m")
    },
    {
        movie: "LOVEní",
        year: "2019",
        duration: moment.duration(106, "m")
    },
    {
        movie: "Řbitov zviřátek",
        year: "2019",
        duration: moment.duration(101, "m")
    },
    {
        movie: "Trabantem tam a zase zpátky ",
        year: "2019",
        duration: moment.duration(114, "m")
    },
    {
        movie: "Hledá se Yetti",
        year: "2019",
        duration: moment.duration(95, "m")
    },
    {
        movie: "High Life",
        year: "2018",
        duration: moment.duration(110, "m")
    },
    {
        movie: "Zatoulaný",
        year: "2018",
        duration: moment.duration(97, "m")
    },
    {
        movie: "Teen Spirit",
        year: "2018",
        duration: moment.duration(92, "m")
    },
    {
        movie: "Stockholm",
        year: "2018",
        duration: moment.duration(92, "m")
    },
    {
        movie: "Dumbo",
        year: "2019",
        duration: moment.duration(112, "m")
    },
    {
        movie: "My",
        year: "2019",
        duration: moment.duration(121, "m")
    },
    {
        movie: "John Wick 3",
        year: "2019",
        duration: moment.duration(130, "m")
    },
    {
        movie: "Free Solo",
        year: "2018",
        duration: moment.duration(100, "m")
    },
    {
        movie: "Raubíř Ralf a internet",
        year: "2018",
        duration: moment.duration(113, "m")
    },
]

//
// Object with screenings
//
class Screenings {
    constructor() {
        // Screenings start today at 9 AM
        this._openHour = 9;
        this._closeHour = 23;
        this._timeSlot = moment("9 00", "HH mm");
        this._tableEl = document.querySelector('#screenings-table');
        // Load screenings in localStorage
        this._load();
        // If screening plan is not long enough, add new screenings
        this._generate();
        // Remove past screenings
        this._filter();
        this._save();
        this.redraw();
    }

    _filter() {
        this._screenings = this._screenings.filter(s => !screeningExpired(s));
    }

    _load() {
        this._screenings = localStorage.screenings ? JSON.parse(localStorage.screenings) : [];
        // Convert date strings to moment objects and remove old screenings
        this._screenings.forEach(s => {
            s.date = moment(s.date);
            s.duration = moment.duration(s.duration);
        });
        // Change next timeslot
        if (this._screenings.length > 0) {
            let last = this._screenings[this._screenings.length - 1];
            this._timeSlot = moment(last.date);
            console.log(this._timeSlot);
            console.log(last);
            this.moveTimeSlot(last.duration);
            console.log(this._timeSlot);
        }
    }

    _save() {
        localStorage.setItem("screenings", JSON.stringify(this._screenings));
    }

    _generate() {
        console.log(this._timeSlot);
        while (this._screenings.length < 10) {
            console.log("Add");
            this.addAll(movies);
        }
    }

    isNextDay() {
        if (this._timeSlot.hour() >= this._closeHour || this._timeSlot.hour() < this._openHour) {
            this._timeSlot.add("10", "H").hour(this._openHour).minute(0).second(0);
            return true;
        }
        return false;
    }

    moveTimeSlot(duration) {
        let start = moment(this._timeSlot);
        // Next screening starts 15 minutes after this movie ends         
        this._timeSlot.add(duration).add(15, "m");
        // If this screening won't fit, move to next day
        if (this.isNextDay()) {
            start = moment(this._timeSlot);
            this._timeSlot.add(duration).add(15, "m");
        }
        return start;
    }

    addScreening(movie) {
        let start = this.moveTimeSlot(movie.duration);
        this._screenings.push({
            date: moment(start),
            movie: movie.movie,
            year: movie.year,
            duration: movie.duration
        });
    }

    addAll(movie_array) {
        movie_array.forEach((val) => {
            this.addScreening(val);
        });
        this._save();
    }

    redraw() {
        generateScreeningPlan(this, this.getScreenings(), this._tableEl);
    }

    // Returns boolean, if screening with given index should be visible
    isFiltered(index) {
        let filter_date = document.querySelector("#filter-date").value;
        let filter_movie = document.querySelector("#filter-movie").value;
        let filter_year = document.querySelector("#filter-year").value;
        let item = this._screenings[index];
        // Filter in range of one day
        if (filter_date != "" && !(moment(filter_date).isBefore(item.date) && moment(filter_date).add(1, "d").isAfter(item.date))) {
            return false;
        } else if (filter_movie != "" && !item.movie.toLowerCase().includes(filter_movie.toLowerCase())) {
            return false;
        } else if (filter_year != "" && filter_year != item.year) {
            return false;
        } else {
            return true;
        }
    }

    getScreenings() {
        return this._screenings;
    }
}

//
// Object for dialog window
//
class Dialog {
    constructor() {
        this._dialogEl = document.querySelector(".dialog");
        this._overlayEl = document.querySelector(".overlay");
    }

    setIcon(htmlData) {
        this._dialogEl.querySelector(".dialog-icon").innerHTML = htmlData;
    }

    setHeader(text) {
        this._dialogEl.querySelector("header h1").textContent = text;
    }

    setContent(htmlData) {
        this._dialogEl.querySelector("section").innerHTML = htmlData;
    }

    show() {
        this._dialogEl.classList.add("dialog-visible");
        this._overlayEl.classList.add("overlay-visible");
    }

    hide() {
        this._dialogEl.classList.remove("dialog-visible");
        this._overlayEl.classList.remove("overlay-visible");
        // If dialog changes URL hash, set it back
        if (location.hash != "") {
            history.pushState("Dialog closed", "", "index.html");
        }
    }
}

class MultiStepDialog extends Dialog {
    constructor(stepsArray) {
        super();
        this._steps = [];
        this._stepIdx = 0;
        this._stepsLen = 0;
        this._next = true;
        this._back = true;
        this._grow = true;
    }

    setSteps(f, len) {
        this._stepFunction = f;
        this._stepsLen = len;
    }

    // Adds back and next arrows to sides using flexbox
    setContent(content) {
        super.setContent(`
        <div class="content">
            ${(this._stepIdx > 0 && this._stepIdx != this._stepsLen - 1) ?
                "<div class='dialog-btn' id='back-btn' onclick='multiDialog.back()'><i class='material-icons md-36'>arrow_back</i></div>" :
                "<div class='dialog-btn' id='back-btn'></div>"
            }
            <div ${this._grow ? "style='flex-grow: 1'" : ""}>${content}</div>
            ${(this._stepIdx < this._stepsLen - 1) ?
                "<div class='dialog-btn' id='next-btn' onclick='multiDialog.next()'><i class='material-icons md-36'>arrow_forward</i></div>" :
                "<div class='dialog-btn' id='next-btn'></div>"
            }
        </div>
        `);
    }

    show() {
        this._stepIdx = 0;
        this.setContent(this._stepFunction(0));
        super.show();
    }

    update() {
        this._dialogEl.classList.remove("dialog-visible");
        this.setContent(this._stepFunction(this._stepIdx));
        this._dialogEl.classList.add("dialog-visible");
    }

    next() {
        if (!this._next) return;
        this._stepIdx++;
        this.update();
    }

    back() {
        if (!this._back) return;
        this._stepIdx--;
        this.update();
    }

    enableNext() {
        this._next = true;
    }

    enableBack() {
        this._back = true;
    }

    disableNext() {
        this._next = false;
    }

    disableBack() {
        this._back = false;
    }

    // If main content should take maximum width
    enableGrow() {
        this._grow = true;
    }

    disableGrow() {
        this._grow = false;
    }
}

// Memory for reserved seats and whole order
class Order {
    constructor() {
        this.reset();
    }

    addSeat(r, s) {
        this._seats.push({
            row: r,
            seat: s
        });
    }

    removeSeat(r, s) {
        this._seats = this._seats.filter(i => (i.row != r) || (i.seat != s));
    }

    setMovie(m) {
        this._movie = m;
    }

    reset() {
        this._seats = [];
        this._movie = {};
    }
}

class Seating {
    constructor(date) {
        this._rows = 5;
        this._seats = 10;
        this._date = date;
        this._plan = [];
        this._generate();
    }

    // Loads or creates seating plan
    _generate() {
        let plan = JSON.parse(localStorage.getItem(this._date.toString()));
        // Plan already in localStorage
        if (plan) {
            console.log("Seating plan loaded from localStorage");
            this._plan = plan;
        } else {
            for (let y = 1; y <= this._rows; ++y) {
                for (let x = 1; x <= this._seats; ++x) {
                    // Back seats are more popular
                    let used = Math.floor((Math.random() * this._rows) + 1) >= y;
                    this._plan.push(used);
                }
            }
            this._savePlan();
        }
    }

    _savePlan() {
        localStorage.setItem(this._date.toString(), JSON.stringify(this._plan));
    }

    // Indexes are expected to start from 1
    _toIndex(row, seat) {
        return (row - 1) * this._seats + (seat - 1);
    }

    isUsed(row, seat) {
        return this._plan[this._toIndex(row, seat)]
    }

    setUsed(row, seat) {
        this._plan[this._toIndex(row, seat)] = true;
        this._savePlan();
    }

    setUnused(row, seat) {
        this._plan[this._toIndex(row, seat)] = false;
        this._savePlan();
    }
}

function screeningExpired(screening) {
    const screenTime = screening.date;
    return screenTime.isBefore();
}

function generateScreeningPlan(object, screenings, targetEl) {
    targetEl.innerHTML = '';

    const screeningsArray = screenings.map((screening, index) => {
        let html = '';
        if (!object.isFiltered(index)) {
            // Item is not filtered -> skip 
            html = '';
        } else if (screeningExpired(screening)) {
            html = `
                    <tr>
                        <td class="date">${ screening.date.fromNow()}</td>
                        <td class="movie">${ screening.movie}</td>
                        <td class="year">${ screening.year}</td>                        
                        <td class="buy">&nbsp</td>
                    </tr>
                `;
        } else {
            html = `
                    <tr>
                        <td class="date">${ screening.date.calendar()}</td>
                        <td class="movie">${ screening.movie}</td>
                        <td class="year">${ screening.year}</td>                        
                        <td class="buy" onclick=buyTicket(${index})><i class="material-icons md-light">shopping_cart</i></td>
                    </tr>
                `;
        }
        return html;
    })
    const screeningsHtml = screeningsArray.join('');
    targetEl.innerHTML = screeningsHtml;
}

function generateSeatingPlan(date) {
    let plan = new Seating(date);
    console.log(plan);
    let html = '';
    for (let y = 1; y <= plan._rows; ++y) {
        html += `<div><span class="row-num">${plan._rows - y + 1}</span>`;
        for (let x = 1; x <= plan._seats; ++x) {
            // Back seats are more popular
            let used = plan.isUsed(y, x);
            let id = "" + y + "_" + x;
            // Modified Google Material Icon (event_seat)            
            html += `
            <input type="checkbox" id="${id}" onchange="buySeat(this)"/>  
            <label for="${id}">              
                <svg class='seat-icon ${used ? "seat-icon-used" : "seat-icon-unused"}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox = "0 0 24 24">
                    <defs>
                        <path id="a" d="M0 0h24v24H0V0z"/>
                    </defs>
                    <clipPath id="b">
                        <use overflow="visible" xlink:href="#a"/>
                    </clipPath>
                    <path d="M4 18v3h3v-3h10v3h3v-6H4zm15-8h3v3h-3zM2 10h3v3H2zm15 3H7V5c0-1.1.9-2 2-2h6c1.1 0 2 .9 2 2v8z" clip-path="url(#b)"/>
                </svg>
            </label>            
            `;
        }
        html += "</div>";
    }
    // Cinema screen 
    html += `
    <br>               
    <svg class='seat-screen' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox = "0 0 250 30">
        <polygon points="29 5, 211 5, 235 24, 5 24" stroke-width="5">
    </svg>
    `
    return html;
}

function buySeat(seatBox) {
    let [row, seat] = seatBox.id.split("_");
    if (seatBox.checked) {
        order.addSeat(row, seat);
    } else {
        order.removeSeat(row, seat);
    }
    // User can continue with at least one seat reserved
    if (order._seats.length > 0) {
        multiDialog.enableNext();
    } else {
        multiDialog.disableNext();
    }
}

function buyingSteps(index) {
    let movie = order._movie;
    switch (index) {
        case 0:
            multiDialog.enableNext();
            multiDialog.enableGrow();
            return `                
                <img src="img/step_1.svg" alt="" style="width: 30vmin; margin: 5vmin;">
                <h1>Vybral jste si následující film</h1>                
                <h2><span class="accent-text-color">${movie.movie}</span> (${movie.date.format("DD.MM.YYYY HH:mm")})</h2>                
                <h5>Na další obrazovce si, prosím, vyberte sedadlo.</h5>
            `;
        case 1:
            multiDialog.disableNext();
            multiDialog.disableGrow();
            return generateSeatingPlan(order._movie.date);
        case 2:
            // Change seating according to order
            let s = new Seating(movie.date);
            order._seats.forEach(seat => s.setUsed(seat.row, seat.seat));
            // Add reservation to localStorage
            let r = JSON.parse(localStorage.getItem("reservations"));
            if (r == null) {
                r = [];
            }
            r.push(order);
            localStorage.setItem("reservations", JSON.stringify(r));
            updateReservationTable();

            let seats = order._seats.length;
            let seatWord = "sedadlo";
            if (seats > 1 && seats < 5) {
                seatWord = "sedadla";
            } else if (seats >= 5) {
                seatWord = "sedadel";
            }
            multiDialog.enableGrow();
            return `
                <img src="img/rocket_launch.svg" alt="" style="width: 45vmin; margin: 5vmin";>
                <h1>Děkujeme za rezervaci.</h1>                
                <h2>Rezervováno: ${seats} ${seatWord}.</h2>                
                <h2>Film <span class="accent-text-color">${order._movie.movie}</span> budeme promítat ${order._movie.date.fromNow()}.</h2>                
                <h3>Vaše rezervace najdete v horní části naší domovské stránky, nad sekcí nabídky filmů.</h3>
            `;
        default:
            return "";
    }
}

function buyTicket(index) {
    // If user reloads page during order, he keeps his order
    if (location.hash == "") {
        // When user uses back button, address already contains hash        
        history.pushState("Buy ticket", "", "#" + index);
    }
    // Ordering in offline mode disabled
    if (!navigator.onLine) {
        dialog.setIcon("cloud_off");
        dialog.setHeader("Offline");
        dialog.setContent(`<img src="img/network.svg" alt="" style="height: 30vmin; margin-right: 5vmin;">
            <br>
            <h1> Momentálně jste offline, nemůžete si tedy zatím objednat žádný lístek. </h1>
        `);
        dialog.show();
        return;
    }
    console.log(screenings.getScreenings()[index]);
    let movie = screenings.getScreenings()[index];
    if (movie == undefined || screeningExpired(movie)) {
        dialog.setIcon("error");
        dialog.setHeader("Chyba");
        if (movie == undefined) {
            dialog.setContent(`<img src="img/developing_code.svg" alt="" style="height: 30vmin;margin-right: 5vmin;">
                <br>
                <h1> Omlouváme se, ale Vámi vybrané promítání neexistuje. </h1>
                <h2> Vyberte si, prosím, jiné z naší nabídky promítání, kterou najdete na naší hlavní stránce. </h2>
            `);
        } else {
            dialog.setContent(`<img src="img/time.svg" alt="" style="height: 30vmin;margin-right: 5vmin;">
                <br>
                <h1> Omlouváme se, ale Vámi vybrané promítání filmu <span class="accent-text-color">${movie.movie}</span> začalo již ${movie.date.fromNow()} </h1>
                <h2> Vyberte si, prosím, jiné z naší nabídky promítání, kterou najdete na naší hlavní stránce. </h2>                
            `);
        }
        dialog.show();
        return;
    }
    // Reset all unsaved orders
    order.reset();
    order.setMovie(movie);
    multiDialog.setSteps(buyingSteps, 3);
    multiDialog.setIcon("local_movies");
    multiDialog.setHeader(screenings.getScreenings()[index].movie);
    multiDialog.show();
}

function updateReservationTable() {
    const tableEl = document.querySelector("#reservations-table");
    let res = localStorage.reservations ? JSON.parse(localStorage.reservations) : [];
    if (res.length == 0) {
        // No reservations -> print nothing
        tableEl.innerHTML = "";
        return;
    }
    let html = `
    <h2 class="section-heading"><span><i class="material-icons md-36">bookmark</i></span>Rezervace<span>&nbsp;</span></h2>
    <table>
        <tr>
            <th class="date">Datum a čas</th>
            <th class="movie">Název filmu</th>
            <th class="seats">Počet sedadel</th>
            <th class="cancel">&nbsp;</th>
        </tr>
    `;
    res.forEach((r, idx) => {
        console.log(r);
        html += `
            <tr id='reservation-${idx}'>
                <td class="date">${ moment(r._movie.date).format("DD.MM.YYYY HH:mm")}</td>
                <td class="movie">${ r._movie.movie}</td>
                <td class="seats">${ r._seats.length}</td>                        
                <td class="cancel" onclick="deleteReservation(${idx})"><i class="material-icons md-light">delete_forever</i></td>
            </tr>
        `;
    });
    html += "</table>";
    tableEl.innerHTML = html;
    return;
}

function deleteReservation(idx) {
    let res = JSON.parse(localStorage.getItem("reservations"));
    if (res == null || res.length == 0) return;
    // Set all reserved seats as unused
    let s = new Seating(moment(res[idx]._movie.date.toString()));
    res[idx]._seats.forEach(seat => s.setUnused(seat.row, seat.seat));
    // Remove reservation from localStorage
    res = res.filter((value, index) => {
        return index != idx;
    });
    localStorage.setItem("reservations", JSON.stringify(res));
    document.querySelector("#reservation-" + idx).classList.add("deleted");
    // Render new table after animation ends (500ms)
    setTimeout(updateReservationTable, 500);
}

// Create table with screenings and reservations
const screenings = new Screenings();
screenings.redraw();
updateReservationTable();

// 
// Listeners
//
// Dialog actions
const dialogEl = document.querySelector(".dialog");
const overlayEl = document.querySelector(".overlay");
const closeBtn = dialogEl.querySelector(".close");
const dialog = new Dialog();
overlayEl.addEventListener("click", (e) => {
    dialog.hide();

});
closeBtn.addEventListener("click", (e) => {
    dialog.hide();
    // Browser would scroll page up, I dont want it.
    e.preventDefault();

});
// Menu actions
const menuEls = document.querySelectorAll("nav a");
const menuElAction = function (event) {
    let icon = this.children[0].textContent;
    let header = this.childNodes[1].textContent;
    dialog.setIcon(icon);
    dialog.setHeader(header);
    if (header.toLowerCase() == "ceník") {
        dialog.setContent(
            `<table>
                <tr><th>Kategorie</th><th>Cena</th></tr>
                <tr><td>Dospělí</td><td>99 Kč</td></tr>
                <tr><td>Studenti a senioři</td><td>69 Kč</td></tr>
                <tr><td>Děti</td><td>49 Kč</td></tr>
            </table>
            <p>* Studenti se musí prokázat platným studentským průkazem ISIC</p>
            <p>** Senioři jsou osoby starší 65 let</p>
            <p>*** Děti jsou osoby mladší 18 let</p>`
        );
    } else if (header.toLowerCase() == "kontakt") {
        dialog.setContent(`<img src="img/astronaut.svg" alt="" style="height: 30vmin; margin-right: 5vmin;"><h2>Toto kino zatím neexistuje, nemůžeme Vám tedy poskytnout žádnou kontaktní adresu, telefon ani email.</h2>
            <h3> Kino bude spuštěno ${moment("2030", "YYYY").fromNow()}. </h3>
            <h5> Děkujeme, Váš MonoKino tým. </h5>
        `);
    }
    dialog.show();
}
// Add all menu items
menuEls.forEach((el) => {
    el.addEventListener("click", menuElAction);
});
// Filter fields
const filterEls = document.querySelectorAll("input.filter");
const filterAction = function (event) {
    screenings.redraw();
    // User might select distant date
    lazyLoad();
}
filterEls.forEach((el) => {
    // Make change after changing/leaving field or pressing a key
    el.addEventListener("input", filterAction);
    el.addEventListener("keyup", filterAction);
});
const historyAction = function (event) {
    console.log(event);
    let hash = location.hash;
    // No hash -> close dialog
    if (hash == "") {
        dialog.hide();
        //multiDialog.hide();
    } else if (hash == "#about" || hash == "#price") {
        // Call function, that is normally called using eventListener
        menuElAction.call(document.querySelector("nav a[href='" + hash + "']"));
    } else if (parseInt(hash.split("#")[1]) != NaN) {
        // User was buying a ticket
        let idx = parseInt(hash.split("#")[1]);
        buyTicket(idx);
    } else {
        console.log(hash);
        history.replaceState("Invalid state", "", "index.html");
    }
}
// Listen for location change
window.addEventListener("popstate", historyAction);
// If user becomes online, reload page
window.addEventListener("online", () => location.reload());
// Back to top scroll button action
function backToTop() {
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}
// Show scroll button only after some scroll distance
window.addEventListener("scroll", (e) => {
    const scrollBtn = document.querySelector("#scroll-button");
    const dist = 1000;
    if (e.pageY > dist || window.scrollY > dist) {
        scrollBtn.classList.remove("scroll-button-hidden");
    } else {
        scrollBtn.classList.add("scroll-button-hidden");
    }
    lazyLoad();
});

// Function adds new screenings if needed
function lazyLoad() {
    while ((window.scrollY + 2 * window.innerHeight) >= document.body.scrollHeight) {
        // User is almost at the end of page, add screenings 
        if (screenings._timeSlot.isBefore(moment().add("1", "M"))) {
            // Add screenings for only one month
            screenings.addAll(movies);
            screenings.redraw();
        } else {
            break;
        }
    }
}

// Multi step dialog
// for ticket reservation
const multiDialog = new MultiStepDialog();
// Order for seat booking
const order = new Order();
// Trigger history change - URL might already contain hash
historyAction(null);